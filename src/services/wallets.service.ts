import {Injectable} from "@angular/core";
import {Wallet} from '../database';

export const StorageKey = "walletID";

@Injectable()
export class WalletService{


  setID(walletID){
    localStorage.setItem(StorageKey,walletID);
  }

  getID() : number{
    return parseInt(localStorage.getItem(StorageKey));
  }

  validateFirstWallet(){
    // Promesa que nosotros retornaremos
    return new Promise((resolve,reject)=>{

      // Búsqueda de la primera cartera
      Wallet.first().then((wallet)=>{
         // Se cumple la promesa de buscar la 1ra cartera
         if(!wallet){
           // Crear primera cartera
           Wallet.createFirst().then((resultado)=>{
             // Promesa de crear 1ra cartera se cumpla
             console.log("Creamos la 1ra cartera");
             this.setID(resultado);
             resolve();
           })
         }else{
           console.log("Ya había una cartera");
           this.setID(wallet.id);
           resolve();
         }
      });
    })
  }
}